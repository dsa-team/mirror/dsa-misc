#!/usr/bin/perl

# Copyright (c) 2008 Peter Palfrader <peter@palfrader.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

use warnings;
use strict;
use IO::Socket::SSL;
use Term::ReadPassword;

my $host = shift;
my $user = shift;
die ("Usage: $0 <host> <user>\n") if (!defined $user || scalar @ARGV);
my $password = read_password("Password for $user\@$host: ");
die ("No password?") unless (defined $password);

$host .= ":443" unless ($host =~ m/:/);

my $client = new IO::Socket::SSL->new(PeerAddr => $host);
die ("Could not connect to/ssl handshake with $host") unless defined $client;
my $msg = <<"EOF";
<?xml version="1.0"?>
	<RIBCL VERSION="2.0">
		<LOGIN USER_LOGIN="$user" PASSWORD="$password">
		<RIB_INFO MODE="write">
			<CERTIFICATE_SIGNING_REQUEST/>
		</RIB_INFO>
	</LOGIN>
</RIBCL>
EOF
for my $line (split /\n/, $msg) {
	print $client $line, "\r\n";
};

while (<$client> !~ /<CERTIFICATE_SIGNING_REQUEST>/) {};
while (my $line = <$client>) {
  last if $line =~ m#</CERTIFICATE_SIGNING_REQUEST>#;
	print $line;
}

# vim:ts=2:
