#!/bin/sh

if [ "$#" != 1 ]; then
  echo >&2 "Usage: $0 <config>"
  exit 1
fi
arg="$1"
cd $(dirname "$arg")
conf=$(basename "$arg")

exec sudo openvpn --config ${conf%.ovpn}.ovpn
