#!/bin/bash

# This script validates that the CSR in question is coming from a legit
# Yubikey with the given serial number.
#
# To create a key:
#
#   ykman -d $serial piv certificates export f9 yubikey-attestation.pem
#   ykman -d $serial piv keys generate -a RSA2048 $slot slot-$slot.pem
#   ykman -d $serial piv keys attest $slot slot-$slot-cert.pem; done

set -e

if [[ $# -ne 4 ]]
then
	echo "Usage: $0 serial yubikey-attestation.pem slot-nn-cert.pem slot-nn-csr.pem" >&2
	exit 1
fi

SCRATCH="$(mktemp -d)"
trap 'rm -rf -- "${SCRATCH}"' EXIT

echo Fetching Yubico CA ...
curl -o "${SCRATCH}/rootca.pem" \
	https://developers.yubico.com/PIV/Introduction/piv-attestation-ca.pem

echo
echo Validating Yubikey attestation ...
openssl verify -CAfile "${SCRATCH}/rootca.pem" "$2"
cat "${SCRATCH}/rootca.pem" "$2" > "${SCRATCH}/chain.pem"

echo -n "Serial: "
SERIAL=$(python3 - <<EOF
import asn1crypto.core
from cryptography import x509
from cryptography.hazmat.backends import default_backend
cert = x509.load_pem_x509_certificate(open('$3', 'rb').read())
print(asn1crypto.core.Integer.load(
	cert.extensions.get_extension_for_oid(x509.ObjectIdentifier('1.3.6.1.4.1.41482.3.7')).value.value).native)
EOF
)
echo -n "${SERIAL}: "
if [[ "${SERIAL}" == "$1" ]]
then
	echo OK
else
	echo NOT OK
	exit 1
fi

echo
echo Validating slot attestation ...
openssl verify -CAfile "${SCRATCH}/chain.pem" "$3"
openssl x509 -noout -subject -in "$3"

echo
echo -n "Validating RSA modulus ... "
ATTESTATION_MODULUS="$(openssl x509 -noout -modulus -in "$3")"
CSR_MODULUS="$(openssl req -in "$4" -noout -modulus)"
if [[ "${ATTESTATION_MODULUS}" == "${CSR_MODULUS}" ]]
then
	echo OK
else
	echo NOT OK
	exit 1
fi

echo
echo Attestation comes from a valid Yubikey and matches the CSR

