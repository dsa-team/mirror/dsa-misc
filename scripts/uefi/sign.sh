#!/bin/bash

# This script signs a given CSR in a way that is compatible with shim.
# It expects the cert and key in ca.pem. openssl pkcs12 is helpful
# for getting here.

cat > openssl.cnf <<EOF
[ca]
default_ca = issuer

[issuer]
private_key = `pwd`/ca.pem
certificate = `pwd`/ca.pem
database = `pwd`/ca.db
serial = `pwd`/ca.srl
default_md = SHA256
new_certs_dir = `pwd`/
policy = no_policy

[no_policy]

[v3_issued]
keyUsage=digitalSignature
extendedKeyUsage=1.3.6.1.5.5.7.3.3
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always
EOF

touch ca.db
touch ca.srl
(dd if=/dev/urandom bs=8 count=1 2> /dev/null) | od -t x1c | head -n 1 | awk '{$1="00";OFS="";print}' > "ca.srl"

openssl ca \
	-batch \
	-config openssl.cnf \
	-cert ca.pem \
	-keyfile ca.pem \
	-days 3650 \
	-preserveDN \
	-rand_serial \
	-extensions v3_issued \
	-in "$1" \
	-out "$(basename $1 .pem)-signed.pem"
