#!/bin/bash

set -e
set -u

VG=vg0
SUITE=wheezy
MIRROR=http://debian.netcologne.de/debian
SPONSOR="[[www.man-da.de|Metropolitan Area Network Darmstadt]] (hosting)"

check_installed() {
    local p
    for p in "$@"; do
        if ! dpkg -l "$p" 2>/dev/null | grep -q '^ii'; then
            echo >&2 "Error: package $p not installed:"
            echo >&2 "  apt-get install $*"
            exit 1
        fi
    done
}
get_sshkey_fprs() {
    local f
    for f in etc/ssh/ssh_host*_key.pub; do
        echo -n "  "
        ssh-keygen -l -f "$f"
    done
}
cleanup() {
    set +x
    if [ -n "${disk:-}" ]; then
        echo "Maybe:"
        echo "lvremove $disk"
    fi
    echo
    if [ -n "${target:-}" ] && [ -e "$target" ]; then
        cd /
        if [ "$(stat -f / -c %i)" != "$(stat -f "$target" -c %i)" ]; then
            umount "$target"
        fi
        rmdir "$target"
    fi

    if [ -n "${part1:-}" ]; then
        kpartx -d -p -p -v "$disk"
    fi
}
check_installed debootstrap debian-archive-keyring kpartx

echo -n "New VM's name: "
if [ -n "${1:-}" ]; then echo "$1"; guest="$1"; shift; else read guest; fi
echo
echo -n "Disk size: [8g]"
if [ -n "${1:-}" ]; then echo "$1"; disksize="$1"; shift; else read disksize; fi
disksize=${disksize:-8g}
thishost=$(hostname)
echo -n "Run on which node? [$thishost]"
if [ -n "${1:-}" ]; then echo "$1"; node="$1"; shift; else read node; fi
node=${node:-$thishost}
echo -n "Secondary node? "
if [ -n "${1:-}" ]; then echo "$1"; secondary="$1"; shift; else read secondary; fi


trap cleanup EXIT

LV="install-$guest"
lvcreate -L "$disksize" -n "$LV" "$VG"

disk="/dev/mapper/$VG-install--$(echo $guest | sed -e 's/-/--/g')"
target="/mnt/target-$guest"

if ! [ -e "$disk" ]; then
    echo >&2 "Error: Disk $disk does not exist."
    exit 1
fi
if [ -e "$target" ]; then
    echo >&2 "Error: Directory $target already exists."
    exit 1
fi

if [ "$(head -c 65536 "$disk" | sha1sum | awk '{print $1}')" != "1adc95bebe9eea8c112d40cd04ab7a8d75c4f961" ]; then
    echo -n "Warning: Disk appears to be not be empty.  Continue anyway? [y/N] "
    read ans
    [ "$ans" = "y" ] || exit 0
fi

echo -n "ipaddr: "; read ipaddr
netmask=255.255.255.192 #echo -n "netmask: "; read netmask
gateway=82.195.75.126   #echo -n "gateway: "; read gateway
ip6addr=2001:41b8:202:deb:1b1b::${ipaddr##*.} # echo -n "ip6addr: "; read ip6addr
ip6gateway=2001:41b8:202:deb::1      # echo -n "ip6gateway: "; read ip6gateway

set -x

if [ -e "$disk-part1" ]; then
    # we already had a partition table and udev/kpartx got it at boot time
    kpartx -v -d -p -part $disk
fi
echo '2048,,L,*' | sfdisk -u S --Linux "$disk"
kpartx -v -p -p -a "$disk"
part1="${disk}-p1"
mkfs.ext4 "$part1"

mkdir "$target"
mount "$part1" "$target"
cd "$target"

debootstrap --variant=minbase --keyring=/usr/share/keyrings/debian-archive-keyring.gpg "$SUITE" . "$MIRROR"

### Set up swap and fstab
dd if=/dev/zero of=swapfile bs=1024k count=512
chmod 0 swapfile
mkswap ./swapfile

uuidroot=$(blkid -s UUID -o value ${part1}) &&
cat > etc/fstab << EOF
UUID=$uuidroot    /               ext4   errors=remount-ro 0       1
/swapfile none swap sw 0 0
EOF
echo 'RAMTMP=yes' >> etc/default/tmpfs

### Set up basic networking stuff
echo "$guest" > etc/hostname
cat > etc/hosts << EOF
127.0.0.1       localhost

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
EOF
rm -fv etc/udev/rules.d/70-persistent-*
mkdir -p etc/udev/rules.d/
touch etc/udev/rules.d/75-persistent-net-generator.rules

cat > etc/network/interfaces << EOF
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address $ipaddr
    netmask $netmask
    gateway $gateway
iface eth0 inet6 static
    address $ip6addr
    gateway $ip6gateway
    netmask 64
    accept_ra 0
EOF

cat > etc/resolv.conf << EOF
nameserver 8.8.8.8
search debian.org
EOF

### A couple packages
mv etc/apt/sources.list etc/apt/sources.list.d/debian.list
chroot . apt-get update
echo "Apt::Install-Recommends 0;" > etc/apt/apt.conf.d/local-recommends
chroot . apt-get install -y locales-all net-tools iproute ifupdown dialog vim netbase udev psmisc usbutils pciutils

### Set up kernel and bootloader
chroot . apt-get install -y linux-image-amd64
DEBIAN_FRONTEND=noninteractive chroot . apt-get install -y grub2

! [ -e dev/vda ]
! [ -e dev/vda1 ]
cp -av `readlink -f "$disk"` dev/new-root
cp -av `readlink -f "$part1"` dev/new-root1
chroot . grub-install --modules=part_msdos /dev/new-root
rm -v dev/new-root*

cp -av `readlink -f "$disk"` dev/vda
cp -av `readlink -f "$part1"` dev/vda1
rm boot/grub/device.map
chroot . update-grub
rm -v dev/vda*

rootpw="$(head -c 12 /dev/urandom | base64)"
echo "root:$rootpw" | chroot . chpasswd

### install ssh
chroot . apt-get install -y ssh
sed -i -e "s/`hostname`\$/$guest/" etc/ssh/ssh_host*_key.pub
sshkeys="$(get_sshkey_fprs)"
rsahostkey="$(cat etc/ssh/ssh_host_rsa_key.pub)"

### clean up
trap - EXIT
cleanup


echo "$guest's root password is $rootpw"
echo "SSH host key fingerprints are:"
echo "$sshkeys"
echo "IP addresses:"
echo "  $ipaddr"
echo "  $ip6addr"

echo
echo "ud-ldap ldpavi snippet:"
cat << EOF
add host=$guest,ou=hosts,dc=debian,dc=org
host: $guest
hostname: $guest.debian.org
objectClass: top
objectClass: debianServer
l: Germany
distribution: Debian GNU/Linux
access: restricted
admin: debian-admin@lists.debian.org
architecture: amd64
sshRSAHostKey: $rsahostkey
ipHostNumber: $ipaddr
ipHostNumber: $ip6addr
mXRecord: 0 INCOMING-MX
physicalHost: ganeti3.debian.org
description: XXX
purpose: XXX
sponsor: $SPONSOR

EOF

echo "Maybe run this now: "
echo " gnt-instance add \\"
echo "    --no-ip-check \\"
echo "    --no-name-check \\"
echo "    --no-start \\"
echo "    -t plain \\"
echo "    --disk=0:adopt=$LV \\"
echo "    --backend-parameters memory=1g \\"
echo "    --os-type debootstrap+default \\"
echo "    -n "${node}" \\"
echo "    $guest.debian.org &&"
echo " gnt-instance modify \\"
echo "    --disk-template drbd \\"
echo "    -n "${secondary}" \\"
echo "    $guest.debian.org &&"
echo " gnt-instance start $guest.debian.org"
