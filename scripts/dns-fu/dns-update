#!/usr/bin/perl
# vim:set ai noet sts=8 ts=8 sw=8 tw=0:
# Local Variables:
# mode:cperl
# cperl-indent-level:4
# End:

# Copyright © Stephen Gran 2009
#
# Author: Stephen Gran <steve@lobefin.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use POSIX qw(strftime);
use File::Temp qw(tempfile);
use DSA::DNS;

my $INDIR = '/var/cache/bind/git/domains';
my $OUTDIR = '/var/cache/bind/generated/';
my @postcommand = qw(sudo /etc/init.d/bind9 reload);
my %vars = (
        minttl     => '1h',
        refresh    => '3h',
        retry      => '1h',
        expire     => '7d',
        negttl     => '1h',
        origin     => 'draghi.debian.org',
        hostmaster => 'hostmaster.debian.org',
);

sub check_files {
        my @cmd = qw(git diff --name-only);
        push @cmd, (join '..', @_);
        open(FILES, '-|', @cmd) or die "git log failed? $!\n";
        my @files = (<FILES>);
        close FILES or die "git log exited non-zero? $!\n";
        chomp(@files);
        return @files;
}

sub write_zonefile {
        my $file = shift;

        open(INFILE, '<', "$INDIR/$file") or die "Can't open $INDIR/$file: $!\n";
	my ($fd, $filename) = tempfile(DIR => $OUTDIR);

	print $fd write_zoneheader($file, $OUTDIR, %vars);
        print $fd $_ while (<INFILE>);
        close $fd;
        close INFILE;
        chmod(0664, $filename);
        rename $filename, "$OUTDIR/$file";
}

sub do_update {
        my @changes = @_;
        delete $ENV{'GIT_DIR'};
        chdir $INDIR or die "chdir $INDIR failed? $!\n";
        my @cmd = qw(git pull);
        system(@cmd) == 0 or die "system @cmd failed: $?\n";

        for my $file (@changes)  {
                next unless ( -f "$INDIR/$file" );
                print "Updating $file ... ";
                write_zonefile($file);
                print "done\n";
        }

        opendir (D, $INDIR) or die "Can't opendir $INDIR: $!\n";
        my @zones = grep { $_ !~ /^\./  && -f "$INDIR/$_" } readdir D;
        closedir D;

        my ($zonefd, $zonefile) = tempfile(DIR => $OUTDIR);

        for my $file (@zones) {
                print $zonefd <<EOF;
zone "$file" {
        type master;
        file "generated/$file";
        allow-query { any; };
        allow-transfer {
                key draghi.debian.org-klecker.debian.org. ;
                key draghi.debian.org-raff.debian.org. ;
                key draghi.debian.org-rietz.debian.org. ;
        };
};

EOF
                unless (-f "$OUTDIR/$file") {
                        print "Updating $file (missing) ... ";
                        write_zonefile($file);
                        print "done\n";
                }
        }

        close $zonefd;
        chmod(0664, $zonefile);
        rename $zonefile, "$OUTDIR/named.conf";

        system(@postcommand) == 0 or die "system @postcommand failed: $?\n";
}

umask(0002);
for my $key (keys %ENV) {
        next if ($key eq 'GIT_DIR');
        delete $ENV{$key};
}
$ENV{'PATH'} = '/bin:/usr/bin:/sbin:/usr/sbin';

my @files;

while (<>) {
        my ($oldrev, $newrev, $refname) = split;
        push @files, (check_files($newrev, $oldrev));
}

my %files;
@files{@files} = ();
do_update(keys %files);

