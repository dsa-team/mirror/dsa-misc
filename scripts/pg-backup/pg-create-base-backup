#!/bin/bash

# Copyright 2007, 2010 Peter Palfrader <peter@palfrader.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# create a backup of the services database, excluding the log (transaction/write ahead logs) directory
# those are handled separately

set -e
set -u

umask 077
myhost="`hostname`"
self="`basename "$0"`[$$]"
date=$(date "+%Y%m%d")

# 3rd argument (port) is obsolete
if [ "$#" != 2 ] && [ "$#" != 3 ] ; then
	echo >&2 "Usage: $self <version> <cluster>"
	exit 1
fi

version="$1"
cluster="$2"

port="$( pg_lsclusters -h | awk '$1=="'"$version"'" && $2=="'"$cluster"'" { print $3 }')"
rootdir="$( pg_lsclusters -h | awk '$1=="'"$version"'" && $2=="'"$cluster"'" { print $6 }')"
dirname="$( dirname "$rootdir" )"
basename="$( basename "$rootdir" )"

if [ "$#" = 3 ] ; then
	if [ "$3" != "$port" ]; then
		echo >&2 "WARNING: $0: Ignoring port number ($3) passed on cmdline."
	fi
fi

info() {
	logger -p daemon.info -t "$self" "$1"
}

croak() {
	logger -s -p daemon.warn -t "$self" "$1"
	exit 1
}

cd /

tmp="`mktemp -d /tmp/pgtar.XXXXXXXX`"
trap "rm -rf '$tmp'" EXIT

label="$date-$myhost-$cluster-$version-backup"
id=`sudo -u postgres psql -p "$port" --tuples-only --command "SELECT pg_start_backup('$label');"`
trap "rm -rf '$tmp'; sudo -u postgres psql -q -p '$port' --command 'SELECT pg_stop_backup();' 2>&1 > /dev/null | grep -v 'NOTICE:  pg_stop_backup complete, all required WAL segments have been archived'" EXIT
id=`echo $id | sed -e 's/[^a-zA-Z0-9]/_/g'`
tarball="$label-$id.tar.gz"

info "Making a base backup of $cluster $version"

cd "$dirname"
rc=0
tar czf "$tmp/$tarball" --exclude "$basename/pg_xlog" "$basename" || rc=$?
if [ "$rc" -eq 0 ]; then
	info "Tar exited successfully"
elif [ "$rc" -eq 1 ]; then
	info "Ignoring tar exit code of 1 (some files changed, but that's ok)"
else
	croak "Tar exited with an exit code $? (not in {0, 1})"
fi

chown postgres "$tmp" "$tmp/$tarball"
sudo -u postgres pg-backup-file "$cluster" BASE "$tmp/$tarball"
